const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql');

const connection = mysql.createConnection({
    host : 'localhost',
    user: 'user_admin',
    password: 'user_admin',
    database: 'hotel_booking_System'
});

connection.connect();
const express = require('express');
const { hash } = require('bcrypt');
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) {return res.sendStatus(403) }
        else{
            req.user = user
            next()
        }
    })
}

/*
Query 1 : list All Registrations

SELECT User.UserId, User.UserName, User.UserSurname, 
    Hotel.HotelName, Registration.RegistrationTime
FROM User, Hotel, Registration
    WHERE (Registration.UserId = User.UserId) AND 
    (Registration.HotelId = Hotel.HotelId)
*/

/*
Query 1 : list All Registrations By Hotel

SELECT User.UserId, User.UserName, User.UserSurname, 
    Hotel.HotelName, Registration.RegistrationTime
FROM User, Hotel, Registration
    WHERE (Registration.UserId = User.UserId) AND 
    (Registration.HotelId = Hotel.HotelId) AND (Hotel.HotelId = 1)
*/

/*
Query 1 : list All Hotel booking by User
SELECT User.UserId, Hotel.HotelName, Registration.RegistrationTime
FROM User, Hotel, Registration
WHERE (User.UserId = Registration.UserId) AND (Registration.HotelId = Hotel.HotelId) AND (User.UserId = 1)

*/

app.get("/list_register", (req, res) => {

    let query = "SELECT * from Registration";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json(rows)
        }
    });
})

/* API for Registering to a new Hotel booking  */
app.post("/register_booking", authenticateToken, (req, res) => {

    let hotel_id = req.query.hotel_id
    let user_id = req.query.user_id

    let query = `INSERT INTO Registration 
                (HotelId, UserId, RegistrationTime) 
                VALUES ('${hotel_id}','${user_id}',NOW() )`
    console.log(query)
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "success" 
            })
        }
    });
})

/* API for Processing User Autorizayion */
app.post("/login", (req, res) => {
    let user_account = req.query.user_account
    let user_password = req.query.user_password
    let query = `SELECT * FROM User WHERE Account='${user_account}'`
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result)=> {
                if(result) {
                    let payload = {
                        "account" : rows[0].Account,
                        "user_id" : rows[0].UserId,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '120m'})
                    res.send(token)
                }else { res.send("Invalid username / password")}
            })
        }
    })
})

/* API for Registering a new user */

app.post("/register_user", (req, res) => {

    let user_name = req.query.user_name
    let user_surname = req.query.user_surname
    let user_account = req.query.user_account
    let user_password = req.query.user_password

    bcrypt.hash(user_password, SALT_ROUNDS, (err, hash) => {
            let query = `INSERT INTO User 
                    (UserName, UserSurname, Account, Password, IsAdmin	) 
                    VALUES ('${user_name}','${user_surname}','${user_account}','${hash}', false)`
        console.log(query)
        
        connection.query( query, (err, rows) => {
            if(err) {
                res.json({
                            "status" : "400",
                            "message" : "Error" 
                        })
            }else {
                res.json({
                    "status" : "200",
                    "message" : "success" 
            })
            }
        });
    })
});


/* DELETE User */
app.post("/delete_user", (req, res) => {

    let user_id = req.query.user_id

    let query = `DELETE FROM User WHERE UserId=${user_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting data into db" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "deleting success" 
        })
        }
    });

})


/* CRUD Operation for Hotel Table */
app.get("/list_hotel", (req, res) =>{
    let query = "SELECT * from Hotel";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json(rows)
        }
    });
})

app.post("/add_hotel", (req, res) => {
    let hotel_name = req.query.hotel_name

    let query = `INSERT INTO Hotel 
                (HotelName) 
                VALUES ('${hotel_name}')`
    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "success" 
        })
        }
    });

})


app.post("/update_hotel", (req, res) => {

    let hotel_id = req.query.hotel_id
    let hotel_name = req.query.hotel_name

    let query = `UPDATE Hotel SET
                HotelName='${hotel_name}', 
                WHERE HotelId=${hotel_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating data" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "updating hotel succesful" 
        })
        }
    });

})

/* DELETE Hotel */
app.post("/delete_hotel", (req, res) => {

    let hotel_id = req.query.hotel_id

    let query = `DELETE FROM Hotel WHERE HotelId=${hotel_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting data into db" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "deleting success" 
        })
        }
    });

})

app.listen(port, () => {
    console.log(`Now starting Running System backend ${port} `)

})


/*query = "SELECT * from Runner";
connection.query( query, (err, rows) => {
    if(err) {
        console.log(err);
    }else {
        console.log(rows);
    }
});

connection.end();*/